package com.c0d3in3.lecture22.main.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.c0d3in3.lecture22.R
import com.c0d3in3.lecture22.main.model.UserModel
import kotlinx.android.synthetic.main.user_item_list.view.*

class UserAdapter(private val items : ArrayList<UserModel>, private val callback: CustomCallback) : RecyclerView.Adapter<UserAdapter.ViewHolder>(){


    interface CustomCallback{
        fun editUser(position: Int)
        fun deleteUser(id : String)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.user_item_list, parent, false))
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind()
    }

    inner class ViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView){
        fun onBind(){
            val model = items[adapterPosition]

            itemView.idTextView.text = "ID: ${model.id}"
            itemView.nameTextView.text = "Name : ${model.name}"
            itemView.jobTextView.text = "Job : ${model.job}"
            itemView.createTextView.text = "${model.createdAt}"

            itemView.editButton.setOnClickListener {
                callback.editUser(adapterPosition)
            }
            itemView.deleteButton.setOnClickListener {
                callback.deleteUser(model.id)
            }
        }
    }
}