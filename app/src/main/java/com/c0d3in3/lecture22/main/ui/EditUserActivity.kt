//package com.c0d3in3.lecture22.main.ui
//
//import android.app.Activity
//import android.content.Intent
//import androidx.appcompat.app.AppCompatActivity
//import android.os.Bundle
//import android.text.Editable
//import android.view.View
//import android.widget.Toast
//import com.c0d3in3.lecture22.R
//import com.c0d3in3.lecture22.main.model.UserModel
//import com.c0d3in3.lecture22.network.USERS
//import com.c0d3in3.lecture22.network.USER_ADD
//import com.c0d3in3.lecture22.network.USER_EDIT
//import com.c0d3in3.lecture22.network.`interface`.ApiCallback
//import com.c0d3in3.lecture22.network.api.ApiHandler
//import com.google.gson.Gson
//import kotlinx.android.synthetic.main.activity_edit_user.*
//
//class EditUserActivity : AppCompatActivity() {
//
//    override fun onCreate(savedInstanceState: Bundle?) {
//        super.onCreate(savedInstanceState)
//        setContentView(R.layout.activity_edit_user)
//
//        init()
//    }
//
//
//
//    private fun init(){
//        val userId = intent.extras?.get("userId")
//
//
//        if(userId == null){
//            titleTextView.text = "Add user"
//            addButton.text = "Add"
//            cancelButton.visibility = View.GONE
//
//            addButton.setOnClickListener {
//
//            }
//
//        }
//        else{
//
//            cancelButton.visibility = View.VISIBLE
//
//            val model = intent.extras!!.get("user") as UserModel
//
//            println(model.name)
//            name.setText(model.name)
//            job.setText(model.job)
//
//            addButton.setOnClickListener {
//
//            }
//
//            cancelButton.setOnClickListener {
//                val intent = Intent(this, MainActivity::class.java)
//                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
//                startActivity(intent)
//            }
//        }
//    }
//}